import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Render
{
  int width;
  int height;
  int pacx;
  int pacy;
  char[][] map;
  Enemies ghosts;

  public Render(File in, Scanner scan)
  {
    width = scan.nextInt();
    height = scan.nextInt();
    map = new char[width][height];
    ghosts = new Enemies();
    init(in, scan);
  }

  public void draw()
  {
    System.out.print(" ");
    for(int i = 0; i < map[0].length; i++)
      System.out.print(i);
    System.out.println();
    for(int w = 0; w < map.length; w++)
    {
      System.out.print(w);
      for(int h = 0; h < map[w].length; h++)
        System.out.print(map[w][h]);
      System.out.println();
    }
  }

  public void init(File in, Scanner scan)
  {
    scan.nextLine();
    int count = 0;
    String line;
    while(scan.hasNext())
    {
      line = scan.nextLine();
      if(line.contains("P"))
      {
        pacx = line.indexOf("P");
        pacy = count;
      }
      if(line.contains("G"))
      {
        ghosts.add(new Ghost("G", line.indexOf('G'), count));
      }
      else if(line.contains("g"))
      {
        ghosts.add(new Ghost("G", line.indexOf('g'), count));
      }
      if(line.contains("H"))
      {
        ghosts.add(new Ghost("H", line.indexOf('H'), count));
      }
      else if(line.contains("h"))
      {
        ghosts.add(new Ghost("H", line.indexOf('h'), count));
      }
      if(line.contains("O"))
      {
        ghosts.add(new Ghost("O", line.indexOf('O'), count));
      }
      else if(line.contains("o"))
      {
        ghosts.add(new Ghost("O", line.indexOf('o'), count));
      }
      if(line.contains("S"))
      {
        ghosts.add(new Ghost("S", line.indexOf('S'), count));
      }
      else if(line.contains("s"))
      {
        ghosts.add(new Ghost("S", line.indexOf('s'), count));
      }
      map[count] = line.toCharArray();
      count++;
    }
  }

  public void update(char pacMove, Render rend)
  {
    switch(pacMove)
    {
      case 'u':
        if(map[pacy][pacx] != '#')
        {
          map[pacy][pacx] = ' ';
          map[pacy - 1][pacx] = 'P';
          pacy--;
        }
        break;
      case 'd':
        if(map[pacy][pacx] != '#')
        {
          map[pacy][pacx] = ' ';
          map[pacy + 1][pacx] = 'P';
          pacy++;
        }
        break;
      case 'l':
        if(map[pacy][pacx] != '#')
        {
          map[pacy][pacx] = ' ';
          map[pacy][pacx - 1] = 'P';
          pacx--;
        }
        break;
      case 'r':
        if(map[pacy][pacx] != '#')
        {
          map[pacy][pacx] = ' ';
          map[pacy][pacx + 1] = 'P';
          pacx++;
        }
        break;
    }
    ghosts.update(pacx, pacy, rend);
  }

  public char get(int x, int y)
  {
    return map[y][x];
  }

  public char[][] getMap()
  {
    return map;
  }
}

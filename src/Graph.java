/*
 * This is from the data type in the book. Should
 * be open...
 <-- here
 * If it's not, I've closed it. Idk why. Page 628.
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class Graph
{
  /* -- Nested classes for Vertices and Edges -- */
  // Vertex (holds the element)
  private class Vertex
  {
    private char element;
    public Vertex up, down, left, right, parent;
    public int xloc, yloc;

    public Vertex(char e, int x, int y)
    {
      element = e;
      xloc = x;
      yloc = y;
    }

    public char getElement()
    {
      return element;
    }
  }

  // Edge class (holds a pair of connected Vertices)
  private class Edge
  {
    private int size;
    private Vertex[] endpoints;

    public Edge(Vertex u, Vertex v, int size)
    {
      endpoints = new Vertex[]{u,v};
      size = 1;
    }

    public int size()
    {
      return size;
    }

    public Vertex[] getEndpoints()
    {
      return endpoints;
    }
  }
  /* ---------- End of nested classes ---------- */

  Vertex[][] lines;
  Vertex ghost;
  private int ghostx, ghosty, pacx, pacy;
  private Render map;

  // Empty constructor
  public Graph(int gx, int gy, int px, int py, Render m)
  {
    ghostx = gx;
    ghosty = gy;
    pacx   = px;
    pacy   = py;
    map    = m;
    lines = new Vertex[map.getMap().length][map.getMap()[0].length];
    ghost = new Vertex(map.get(ghostx, ghosty), ghostx, ghosty);
    build();
  }

  public Edge getEdge(Vertex u, Vertex v)
  {
    if(u.up == v)
      return new Edge(u, v, 1);
    return null;
  }

  public Vertex[] endVertices(Edge e)
  {
    return e.getEndpoints();
  }

  public void build()
  {
    int height = lines.length, width = lines[0].length;
    for(int h = 0; h < height; h++)
      for(int w = 0; w < width; w++)
      {
        lines[h][w] = new Vertex(map.get(w, h), w, h);
      }

    for(int h = 1; h < height - 1; h++)
      for(int w = 1; w < width - 1; w++)
      {
        if(map.get(w, h) == map.get(ghostx, ghosty))
        {
          lines[h][w] = ghost;
          ghost = lines[h][w];
        }
        lines[h][w].up = lines[h - 1][w];
        lines[h][w].right = lines[h][w + 1];
        lines[h][w].down = lines[h + 1][w];
        lines[h][w].left = lines[h][w - 1];
      }
  }

  public void BFS(int pacx, int pacy)
  {
    LinkedList<Vertex> queue = new LinkedList<>();
    ArrayList<Vertex> visited = new ArrayList<>();
    ArrayList<Vertex> path = new ArrayList<>();
    Vertex pacman = null;
    char move = ' ';
    queue.add(ghost);

    while(!queue.isEmpty())
    {
      Vertex curr = queue.remove();

      if(!visited.contains(curr.up) && curr.up != null)
      {
        switch(curr.up.getElement())
        {
          case '#':
            break;
          case 'P':
            pacman = curr.up;
            path.add(curr);
            while(curr.parent != ghost)
            {
              path.add(curr.parent);
              curr = curr.parent;
            }
            Collections.reverse(path);
            break;
          default:
            visited.add(curr);
            curr.up.parent = curr;
            queue.add(curr.up);
            break;
        }
      }
      if(path.size() != 0)
        break;

      if(!visited.contains(curr.right) && curr.right != null)
      {
        switch(curr.right.getElement())
        {
          case '#':
            break;
          case 'P':
            pacman = curr.right;
            path.add(curr);
            while(curr.parent != ghost)
            {
              path.add(curr.parent);
              curr = curr.parent;
            }
            Collections.reverse(path);
            pacman = curr.right;
            break;
          default:
            visited.add(curr);
            curr.right.parent = curr;
            queue.add(curr.right);
            break;
        }
      }
      if(path.size() != 0)
        break;

      if(!visited.contains(curr.down) && curr.down != null)
      {
        switch(curr.down.getElement())
        {
          case '#':
            break;
          case 'P':
            pacman = curr.down;
            path.add(curr);
            while(curr.parent != ghost)
            {
              path.add(curr.parent);
              curr = curr.parent;
            }
            Collections.reverse(path);
            pacman = curr.down;
            break;
          default:
            visited.add(curr);
            curr.down.parent = curr;
            queue.add(curr.down);
            break;
        }
      }
      if(path.size() != 0)
        break;

      if(!visited.contains(curr.left) && curr.left != null)
      {
        switch(curr.left.getElement())
        {
          case '#':
            break;
          case 'P':
            pacman = curr.left;
            path.add(curr);
            while(curr.parent != ghost)
            {
              path.add(curr.parent);
              curr = curr.parent;
            }
            Collections.reverse(path);
            pacman = curr.left;
            break;
          default:
            visited.add(curr);
            curr.left.parent = curr;
            queue.add(curr.left);
            break;
        }
      }
      if(path.size() != 0)
        break;
    }

    if(path.get(0) == ghost.up)
      move = 'u';
    else if(path.get(0) == ghost.right)
      move = 'r';
    else if(path.get(0) == ghost.down)
      move = 'd';
    else if(path.get(0) == ghost.left)
      move = 'l';
    System.out.print("Ghost " + ghost.getElement() + ": " + move +
                       " " + (path.size() + 1) + " ");
    System.out.print("(" + ghosty + "," + ghostx + ") ");
    for(int i = 0; i < path.size(); i++)
      System.out.print("(" + path.get(i).yloc + "," +
                       path.get(i).xloc + ") ");
    System.out.println("(" + pacy + "," + pacx + ")");
  }
}

public class Ghost implements Comparable<Ghost>
{
  private int ghostx, ghosty; // ghost coords
  public char dotG;          // ghost with a dot behind
  private char clrG;          // ghost with clear space behind
  private Graph path;

  public Ghost(String name, int x, int y)
  {
    ghostx = x;
    ghosty = y;
    dotG = name.charAt(0);
    clrG = name.toLowerCase().charAt(0);
  }

  public void move(int pacx, int pacy, Render map)
  {
    path = new Graph(ghostx, ghosty, pacx, pacy, map);
    path.BFS(pacx, pacy);
  }

  public int compareTo(Ghost g)
  {
    if(g.dotG < this.dotG)
      return 1;
    else
      return -1;
  }
}

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Enemies implements Iterable<Ghost>
{
  private ArrayList<Ghost> enemies;

  public Enemies()
  {
    enemies = new ArrayList<Ghost>();
  }

  public void add(Ghost g)
  {
    enemies.add(g);
    Collections.sort(enemies);
  }

  public void update(int pacx, int pacy, Render map)
  {
    for(Ghost g : enemies)
    {
      g.move(pacx, pacy, map);
    }
  }

  @Override
  public Iterator<Ghost> iterator()
  {
    Iterator<Ghost> iter = enemies.iterator();
    return iter;
  }
}

/*

  Author: Calvin Williams
  Email: cwilliams2016@my.fit.edu
  Course: Algorithms and Data Structures
  Section: 3
  Description: Pac-man

*/

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class HW6
{

  /*
    A single round of a game of Pac-man
  */
  public static void main(String[] args) throws IOException
  {
    File map = new File(args[0]);
    Scanner scan = new Scanner(map);
    Scanner input = new Scanner(System.in);
    Render screen = new Render(map, scan);
    char move;
    int turn = 0, max = 1;
    while(turn < max)
    {
      screen.draw();
      System.out.print("Please enter your move [u(p), d(own), l(eft), or r(ight)]: ");
      move = input.next().charAt(0);
      System.out.println("Points: 1");
      screen.update(move, screen);
      screen.draw();
      turn++;
    }
    scan.close();
    input.close();
  }

}
